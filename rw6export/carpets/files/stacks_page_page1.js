
// 'stacks' is the Stacks global object.
// All of the other Stacks related Javascript will 
// be attatched to it.
var stacks = {};


// this call to jQuery gives us access to the globaal
// jQuery object. 
// 'noConflict' removes the '$' variable.
// 'true' removes the 'jQuery' variable.
// removing these globals reduces conflicts with other 
// jQuery versions that might be running on this page.
stacks.jQuery = jQuery.noConflict(true);

// Javascript for com_joeworkman_stacks_glider
// ---------------------------------------------------------------------

// Each stack has its own object with its own namespace.  The name of
// that object is the same as the stack's id.
stacks.com_joeworkman_stacks_glider = {};

// A closure is defined and assigned to the stack's object.  The object
// is also passed in as 'stack' which gives you a shorthand for referring
// to this object from elsewhere.
stacks.com_joeworkman_stacks_glider = (function(stack) {

	// When jQuery is used it will be available as $ and jQuery but only
	// inside the closure.
	var jQuery = stacks.jQuery;
	var $ = jQuery;
	
!function(a){"use strict";var b=function(a){var b=a.replace(/\s/g,"").match(/^rgb\((\d+)\,(\d+)\,(\d+)\)/);if(b)return{r:parseInt(b[1]),g:parseInt(b[2]),b:parseInt(b[3])};var c=/^#?([a-f\d])([a-f\d])([a-f\d])$/i;a=a.replace(c,function(a,b,c,d){return b+b+c+c+d+d});var d=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(a);return d?{r:parseInt(d[1],16),g:parseInt(d[2],16),b:parseInt(d[3],16)}:null},c=function(a,b,c){if(b.length!==c.length)return console.error("glass: Could not create gradient string. The number stops and colors do not match."),!1;for(var d=[a],e=0;e<c.length;e++)d.push(b[e]+" "+c[e]+"%");return d.join(",")},d=function(b,d,e,g){var h=c(b.direction,e,g);if(b.stylesheet){var i="background:-webkit-linear-gradient("+h+");background:-moz-linear-gradient("+h+");background:-o-linear-gradient("+h+");background:linear-gradient("+h+");";f(b.stylesheet,i)}return h&&d?b.hover?a(d).hover(function(){a(d).css("background","-webkit-linear-gradient("+h+")").css("background","-moz-linear-gradient("+h+")").css("background","-o-linear-gradient("+h+")").css("background","linear-gradient("+h+")")},function(){a(d).css("background","")}):a(d).css("background","-webkit-linear-gradient("+h+")").css("background","-moz-linear-gradient("+h+")").css("background","-o-linear-gradient("+h+")").css("background","linear-gradient("+h+")"):!1},e=function(a,b,c){return c="undefined"!=typeof c?c:0,b="undefined"!=typeof b?b:1,b=b>1?b/100:b,"rgba("+(a.r+c)+","+(a.g+c)+","+(a.b+c)+","+b+")"},f=function(b,c){var d=0,e=a.fn.glass.stylesheet;e.insertRule?e.insertRule(b+"{"+c+"}",d):e.addRule(b,c,d)};a.glass=function(c){var d=a.extend(!0,{},a.fn.glass.defaults,c||{});if(d.color=b(d.color),d.stylesheet){var e=a.fn.glass[d.gradient];"function"==typeof e&&e(d,!1)}},a.fn.glass=function(c){var d=a.extend(!0,{},a.fn.glass.defaults,c||{});return d.color=b(d.color),this.each(function(){var b=a(this),c=a.fn.glass[d.gradient];"function"==typeof c&&c(d,b)})},a.fn.glass.solid=function(b,c){var d=e(b.color,b.opacity);return b.stylesheet&&f(b.stylesheet,"background:"+d+";"),c?b.hover?a(c).hover(function(){a(c).css("background",d)},function(){a(c).css("background","")}):a(c).css("background",d):!1},a.fn.glass.twocolor=function(a,c){var f=[],g=[0,85],h=b(a.rules.twocolor.top);return f.push(e(h,a.opacity)),f.push(e(a.color,a.opacity)),d(a,c,f,g)},a.fn.glass.faded=function(a,b){var c=[],f=[0,85];return c.push(e(a.color,a.opacity)),c.push(e(a.color,a.rules.faded.bottom)),d(a,b,c,f)},a.fn.glass.twotone=function(a,b){var c=[],f=[0,45,50,50,100],g=a.rules.onecolor.offsets;g.length!==f.length&&console.warn("glass twotone: The gradient may not turn out what you expect, the number of offsets do not match the number of stops"),g[3]=0;for(var h=0;h<f.length;h++){var i=a.rules.twotone.offsets[h]||0;c.push(e(a.color,a.opacity,i))}return d(a,b,c,f)},a.fn.glass.onecolor=function(a,b){var c=[],f=a.rules.onecolor.stops,g=a.rules.onecolor.offsets;g.length!==f.length&&console.warn("glass onecolor: The gradient may not turn out what you expect, the number of offsets do not match the number of stops");for(var h=0;h<f.length;h++){var i=g[h]||0;c.push(e(a.color,a.opacity,i))}return d(a,b,c,f)},a.fn.glass.stylesheet=function(){var a=document.createElement("style");return a.appendChild(document.createTextNode("")),document.head.appendChild(a),a.sheet}(),a.fn.glass.defaults={color:"#ffffff",gradient:"solid",direction:"top",opacity:100,stylesheet:!1,hover:!1,rules:{onecolor:{offsets:[40,0],stops:[0,85]},twocolor:{top:"#777777"},twotone:{offsets:[20,10,5,0,-10]},faded:{bottom:80}}}}(jQuery);

$(document).ready(function(){var openClass='opened';stacks.com_joeworkman_stacks_glider.closeAllGliders=function(){$('.com_joeworkman_stacks_glider_stack .glider-wrapper').removeClass(openClass);$('.com_joeworkman_stacks_glider_stack .glider-button').removeClass(openClass);};stacks.com_joeworkman_stacks_glider.toggleGlider=function(event){event.stopPropagation();var gliderID=$(this).data('glider')||$(this).attr('href').substring(1),glider=$('#'+gliderID),container=$('.glider-wrapper',glider),button=$('.glider-button',glider);if(glider.length>0){if(container.hasClass(openClass)){stacks.com_joeworkman_stacks_glider.closeAllGliders();}
else{stacks.com_joeworkman_stacks_glider.closeAllGliders();container.addClass(openClass);button.addClass(openClass);}}
else{console.error('Unable to locate glider instance with ID: '+gliderID);}
$(window).trigger('resize');return false;};$('.glider-toggle,.glider-toggle a').click(stacks.com_joeworkman_stacks_glider.toggleGlider);$('html').click(stacks.com_joeworkman_stacks_glider.closeAllGliders);});

	return stack;
})(stacks.com_joeworkman_stacks_glider);


// Javascript for com_joeworkman_stacks_foundation_styles
// ---------------------------------------------------------------------

// Each stack has its own object with its own namespace.  The name of
// that object is the same as the stack's id.
stacks.com_joeworkman_stacks_foundation_styles = {};

// A closure is defined and assigned to the stack's object.  The object
// is also passed in as 'stack' which gives you a shorthand for referring
// to this object from elsewhere.
stacks.com_joeworkman_stacks_foundation_styles = (function(stack) {

	// When jQuery is used it will be available as $ and jQuery but only
	// inside the closure.
	var jQuery = stacks.jQuery;
	var $ = jQuery;
	
 jQuery.fn.exists=function(){return jQuery(this).length>0;}
jQuery.debug=function(msg,obj){if(window.debug===true&&window.console&&console.log){console.log('[jwstacks] '+msg);if(obj)console.log(obj);}}
jQuery.isMobile=function(){return'ontouchstart'in window||'onmsgesturechange'in window;};
!function(a){"use strict";var b=function(a){var b=a.replace(/\s/g,"").match(/^rgb\((\d+)\,(\d+)\,(\d+)\)/);if(b)return{r:parseInt(b[1]),g:parseInt(b[2]),b:parseInt(b[3])};var c=/^#?([a-f\d])([a-f\d])([a-f\d])$/i;a=a.replace(c,function(a,b,c,d){return b+b+c+c+d+d});var d=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(a);return d?{r:parseInt(d[1],16),g:parseInt(d[2],16),b:parseInt(d[3],16)}:null},c=function(a,b,c){if(b.length!==c.length)return console.error("glass: Could not create gradient string. The number stops and colors do not match."),!1;for(var d=[a],e=0;e<c.length;e++)d.push(b[e]+" "+c[e]+"%");return d.join(",")},d=function(b,d,e,g){var h=c(b.direction,e,g);if(b.stylesheet){var i="background:-webkit-linear-gradient("+h+");background:-moz-linear-gradient("+h+");background:-o-linear-gradient("+h+");background:linear-gradient("+h+");";f(b.stylesheet,i)}return h&&d?b.hover?a(d).hover(function(){a(d).css("background","-webkit-linear-gradient("+h+")").css("background","-moz-linear-gradient("+h+")").css("background","-o-linear-gradient("+h+")").css("background","linear-gradient("+h+")")},function(){a(d).css("background","")}):a(d).css("background","-webkit-linear-gradient("+h+")").css("background","-moz-linear-gradient("+h+")").css("background","-o-linear-gradient("+h+")").css("background","linear-gradient("+h+")"):!1},e=function(a,b,c){return c="undefined"!=typeof c?c:0,b="undefined"!=typeof b?b:1,b=b>1?b/100:b,"rgba("+(a.r+c)+","+(a.g+c)+","+(a.b+c)+","+b+")"},f=function(b,c){var d=0,e=a.fn.glass.stylesheet;e.insertRule?e.insertRule(b+"{"+c+"}",d):e.addRule(b,c,d)};a.glass=function(c){var d=a.extend(!0,{},a.fn.glass.defaults,c||{});if(d.color=b(d.color),d.stylesheet){var e=a.fn.glass[d.gradient];"function"==typeof e&&e(d,!1)}},a.fn.glass=function(c){var d=a.extend(!0,{},a.fn.glass.defaults,c||{});return d.color=b(d.color),this.each(function(){var b=a(this),c=a.fn.glass[d.gradient];"function"==typeof c&&c(d,b)})},a.fn.glass.solid=function(b,c){var d=e(b.color,b.opacity);return b.stylesheet&&f(b.stylesheet,"background:"+d+";"),c?b.hover?a(c).hover(function(){a(c).css("background",d)},function(){a(c).css("background","")}):a(c).css("background",d):!1},a.fn.glass.twocolor=function(a,c){var f=[],g=[0,85],h=b(a.rules.twocolor.top);return f.push(e(h,a.opacity)),f.push(e(a.color,a.opacity)),d(a,c,f,g)},a.fn.glass.faded=function(a,b){var c=[],f=[0,85];return c.push(e(a.color,a.opacity)),c.push(e(a.color,a.rules.faded.bottom)),d(a,b,c,f)},a.fn.glass.twotone=function(a,b){var c=[],f=[0,45,50,50,100],g=a.rules.onecolor.offsets;g.length!==f.length&&console.warn("glass twotone: The gradient may not turn out what you expect, the number of offsets do not match the number of stops"),g[3]=0;for(var h=0;h<f.length;h++){var i=a.rules.twotone.offsets[h]||0;c.push(e(a.color,a.opacity,i))}return d(a,b,c,f)},a.fn.glass.onecolor=function(a,b){var c=[],f=a.rules.onecolor.stops,g=a.rules.onecolor.offsets;g.length!==f.length&&console.warn("glass onecolor: The gradient may not turn out what you expect, the number of offsets do not match the number of stops");for(var h=0;h<f.length;h++){var i=g[h]||0;c.push(e(a.color,a.opacity,i))}return d(a,b,c,f)},a.fn.glass.stylesheet=function(){var a=document.createElement("style");return a.appendChild(document.createTextNode("")),document.head.appendChild(a),a.sheet}(),a.fn.glass.defaults={color:"#ffffff",gradient:"solid",direction:"top",opacity:100,stylesheet:!1,hover:!1,rules:{onecolor:{offsets:[40,0],stops:[0,85]},twocolor:{top:"#777777"},twotone:{offsets:[20,10,5,0,-10]},faded:{bottom:80}}}}(jQuery);

	return stack;
})(stacks.com_joeworkman_stacks_foundation_styles);


// Javascript for com_joeworkman_stacks_foundation_topbar
// ---------------------------------------------------------------------

// Each stack has its own object with its own namespace.  The name of
// that object is the same as the stack's id.
stacks.com_joeworkman_stacks_foundation_topbar = {};

// A closure is defined and assigned to the stack's object.  The object
// is also passed in as 'stack' which gives you a shorthand for referring
// to this object from elsewhere.
stacks.com_joeworkman_stacks_foundation_topbar = (function(stack) {

	// When jQuery is used it will be available as $ and jQuery but only
	// inside the closure.
	var jQuery = stacks.jQuery;
	var $ = jQuery;
	
;(function($){"use strict";var version='20140730.1100',debug=function(msg,obj){if(window.debug===true&&window.console&&console.log){console.log('[rwflexnav] '+msg);if(obj)console.log(obj);}};$.fn.divideNav=function(options){var opts=$.extend(true,{},$.fn.rwflexnav.defaults,options||{});var container=$(this);$('> li.'+opts.className.divider,container).remove();var items=$('> li',container);$('<li>').addClass(opts.className.divider).insertBefore(items);$('<li>').addClass(opts.className.divider).insertAfter(items.last());return $(container);};$.fn.hackRWFlexNav=function(options){var opts=$.extend(true,{},$.fn.rwflexnav.defaults,options||{});var container=$(this);$('> ul.'+opts.className.menu,container).first().removeClass(opts.className.dropdown).find('ul').removeClass(opts.className.menu);$('> ul.'+opts.className.menu,container).find('li').each(function(){if($(this).find('ul').length===0)$(this).removeClass(opts.className.hasDropdown);});return $(container);};$.fn.processRWFlexNav=function(options){var opts=$.extend(true,{},$.fn.rwflexnav.defaults,options||{});var container=$(this);$('li a header',container).parent().attr('href','javascript:void(0)').parent().addClass(opts.className.heading);$('li a label',container).unwrap();$('li a button',container).each(function(){var classes=$(this).attr('class');var text=$(this).text();$(this).parent().addClass(opts.className.button).addClass(classes).html(text).parent().addClass(opts.className.hasButton);});$('li a hr',container).unwrap().parent().addClass(opts.className.divider).html('');return $(container);};$.fn.rwflexnav=function(options){var opts=$.extend(true,{},$.fn.rwflexnav.defaults,options||{});return this.each(function(){if($('> ul.'+opts.className.menu,this).length===0){console.error('RWFlexNav Aborting! Unable to locate menu with class "'+opts.className.menu+'"');return false;}
var method=$.fn.rwflexnav[opts.scope],container=typeof method==='function'?method(opts,this):false;if(opts.hackNav)container.hackRWFlexNav(opts);container.processRWFlexNav(opts);if(opts.divide)$('> ul.'+opts.className.menu,container).divideNav(opts);debug('Processing Complete',container);return $(container);});};$.fn.rwflexnav.all=function(opts,container){return $(container);};$.fn.rwflexnav.top=function(opts,container){$('> ul.'+opts.className.menu,container).find('ul').remove();return $(container);};$.fn.rwflexnav.subnav=function(opts,container){var menu=$('> ul.'+opts.className.menu,container).first(),submenu=$('> ul.'+opts.className.menu+'> li:nth-child('+opts.subNavIndex+') > ul',container),classes=menu.attr('class');debug('Scope SubNav',submenu);if(submenu.length===0){console.error('RWFlexNav Error! No subnav found at index '+opts.subNavIndex);}
else{submenu.addClass(opts.className.menu).addClass(classes).insertAfter(menu);submenu.find('>li.js-generated').remove();menu.remove();}
return $(container);};$.fn.rwflexnav.active=function(opts,container){var menu=$('> ul.'+opts.className.menu,container).first(),submenu=$('> ul.'+opts.className.menu+' > li.'+opts.className.active+' > ul',container),classes=menu.attr('class');debug('Scope Active',submenu);if(submenu.length===0){console.error('RWFlexNav Error! No active subnav found with class "'+opts.className.active+'"');}
else{submenu.addClass(opts.className.menu).addClass(classes).insertAfter(menu);submenu.find('>li.js-generated').remove();menu.remove();}
return $(container);};$.fn.rwflexnav.defaults={scope:'all',hackNav:true,divide:false,subNavIndex:0,className:{menu:'menu',heading:'heading',divider:'divider',active:'active',dropdown:'dropdown',button:'button',hasButton:'has-form',hasDropdown:'has-dropdown'}};})(jQuery);

	return stack;
})(stacks.com_joeworkman_stacks_foundation_topbar);


// Javascript for stacks_in_26_page1
// ---------------------------------------------------------------------

// Each stack has its own object with its own namespace.  The name of
// that object is the same as the stack's id.
stacks.stacks_in_26_page1 = {};

// A closure is defined and assigned to the stack's object.  The object
// is also passed in as 'stack' which gives you a shorthand for referring
// to this object from elsewhere.
stacks.stacks_in_26_page1 = (function(stack) {

	// When jQuery is used it will be available as $ and jQuery but only
	// inside the closure.
	var jQuery = stacks.jQuery;
	var $ = jQuery;
	


	return stack;
})(stacks.stacks_in_26_page1);


// Javascript for stacks_in_4302_page1
// ---------------------------------------------------------------------

// Each stack has its own object with its own namespace.  The name of
// that object is the same as the stack's id.
stacks.stacks_in_4302_page1 = {};

// A closure is defined and assigned to the stack's object.  The object
// is also passed in as 'stack' which gives you a shorthand for referring
// to this object from elsewhere.
stacks.stacks_in_4302_page1 = (function(stack) {

	// When jQuery is used it will be available as $ and jQuery but only
	// inside the closure.
	var jQuery = stacks.jQuery;
	var $ = jQuery;
	
$(document).ready(function(){var jack=$("#stacks_in_4302_page1 >.jack-wrapper");var trigger=$('>.jack,>.jack-back',jack);jack.hover(function(){trigger.addClass('hover');},function(){trigger.removeClass('hover');});});

	return stack;
})(stacks.stacks_in_4302_page1);


// Javascript for stacks_in_29_page1
// ---------------------------------------------------------------------

// Each stack has its own object with its own namespace.  The name of
// that object is the same as the stack's id.
stacks.stacks_in_29_page1 = {};

// A closure is defined and assigned to the stack's object.  The object
// is also passed in as 'stack' which gives you a shorthand for referring
// to this object from elsewhere.
stacks.stacks_in_29_page1 = (function(stack) {

	// When jQuery is used it will be available as $ and jQuery but only
	// inside the closure.
	var jQuery = stacks.jQuery;
	var $ = jQuery;
	
 $(document).ready(function(){var stack=$('#stacks_in_29_page1'),opacity=100,opacityBottom=85,hoverOpacity=15,subNavIndex=1,divide=1,zoneDivider=1,fullWidthMenu='',scope='all',style='solid',menuAlign='centered',baseColor='#333333',baseColorTop='#737373',hoverColor='#FFFFFF',rwmenu='rw'==='rw'?true:false;stack.parent().css({overflow:'visible'});$('.top-bar-section:not(.menu-magellan):not(.menu-none)',stack).rwflexnav({scope:scope,subNavIndex:subNavIndex,divide:divide});if($('ul.magellanList',stack).exists()){if(divide)$('ul.magellanList',stack).divideNav();$('.magellanList li',stack).each(function(){var id=$(this).data('magellan-arrival');$('a',this).attr('href','#'+id);});}
if($('ul.zone',stack).exists()){if(zoneDivider)$('ul.zone',stack).divideNav();}
if($('.zone-align-left ul.zone',stack).exists()||!$('ul.zone',stack).exists()){$('ul.menu',stack).addClass(menuAlign);}
if(fullWidthMenu==='full-width'&&menuAlign==='centered'){var calcMenuItemWidth=function(){var items=$('.menu-align-centered ul.menu > li:not(.divider)',stack),dividers=$('.menu-align-centered ul.menu > li.divider',stack);if($('.top-bar .toggle-topbar',stack).is(':visible')){items.removeAttr('style');}
else{var count=items.length;if(count>0){var width=dividers.length>0?100/count-0.2:100/count;items.width(width+"%");}}};calcMenuItemWidth();$(window).on('resize',calcMenuItemWidth);}
if(opacity<100||style=="faded"){$('.top-bar-wrapper',stack).glass({color:baseColor,gradient:style,opacity:opacity,rules:{twocolor:{top:baseColorTop},faded:{bottom:opacityBottom}}});$('ul.dropdown',stack).glass({color:baseColor,opacity:opacity});}
if(hoverOpacity<100){var menuItems,dropdowns;if($('.top-bar-wrapper',stack).hasClass('styleActive')){menuItems=$('ul:not(.title-area) li:not(.has-dropdown):not(.active) > a:not(.button)',stack);dropdowns=$('ul:not(.title-area) li.has-dropdown:not(.active)',stack);if($('.magellanList',stack).exists()){$.glass({color:hoverColor,opacity:hoverOpacity,stylesheet:'#stacks_in_29_page1 .top-bar-wrapper.styleActive .magellanList li.active > a'});}
else{$('li.active > a',stack).glass({color:hoverColor,opacity:hoverOpacity});}}
else{menuItems=$('ul:not(.title-area) li:not(.has-dropdown) > a:not(.button)',stack);dropdowns=$('ul:not(.title-area) li.has-dropdown',stack);}
menuItems.glass({color:hoverColor,opacity:hoverOpacity,hover:true});dropdowns.hover(function(){$('> a:not(.button)',this).glass({color:hoverColor,opacity:hoverOpacity});},function(){$('> a:not(.button)',this).css('background','');});}});

	return stack;
})(stacks.stacks_in_29_page1);


// Javascript for stacks_in_4312_page1
// ---------------------------------------------------------------------

// Each stack has its own object with its own namespace.  The name of
// that object is the same as the stack's id.
stacks.stacks_in_4312_page1 = {};

// A closure is defined and assigned to the stack's object.  The object
// is also passed in as 'stack' which gives you a shorthand for referring
// to this object from elsewhere.
stacks.stacks_in_4312_page1 = (function(stack) {

	// When jQuery is used it will be available as $ and jQuery but only
	// inside the closure.
	var jQuery = stacks.jQuery;
	var $ = jQuery;
	
$(document).ready(function(){var jack=$("#stacks_in_4312_page1 >.jack-wrapper");var trigger=$('>.jack,>.jack-back',jack);jack.hover(function(){trigger.addClass('hover');},function(){trigger.removeClass('hover');});});

	return stack;
})(stacks.stacks_in_4312_page1);


// Javascript for stacks_in_5036_page1
// ---------------------------------------------------------------------

// Each stack has its own object with its own namespace.  The name of
// that object is the same as the stack's id.
stacks.stacks_in_5036_page1 = {};

// A closure is defined and assigned to the stack's object.  The object
// is also passed in as 'stack' which gives you a shorthand for referring
// to this object from elsewhere.
stacks.stacks_in_5036_page1 = (function(stack) {

	// When jQuery is used it will be available as $ and jQuery but only
	// inside the closure.
	var jQuery = stacks.jQuery;
	var $ = jQuery;
	

$(document).ready(function(){var stack=$('#stacks_in_5036_page1'),container=$('.glider-wrapper',stack),button=$('.glider-button',stack);container.glass({color:container.css('background-color'),gradient:'solid',opacity:90});button.glass({color:button.css('background-color'),gradient:'solid',opacity:80});container.click(function(event){event.stopPropagation()});});

	return stack;
})(stacks.stacks_in_5036_page1);


// Javascript for stacks_in_5007_page1
// ---------------------------------------------------------------------

// Each stack has its own object with its own namespace.  The name of
// that object is the same as the stack's id.
stacks.stacks_in_5007_page1 = {};

// A closure is defined and assigned to the stack's object.  The object
// is also passed in as 'stack' which gives you a shorthand for referring
// to this object from elsewhere.
stacks.stacks_in_5007_page1 = (function(stack) {

	// When jQuery is used it will be available as $ and jQuery but only
	// inside the closure.
	var jQuery = stacks.jQuery;
	var $ = jQuery;
	

$(document).ready(function(){var stack=$('#stacks_in_5007_page1'),container=$('.glider-wrapper',stack),button=$('.glider-button',stack);container.glass({color:container.css('background-color'),gradient:'solid',opacity:90});button.glass({color:button.css('background-color'),gradient:'onecolor',opacity:80});container.click(function(event){event.stopPropagation()});});

	return stack;
})(stacks.stacks_in_5007_page1);


